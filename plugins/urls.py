from django.conf.urls import url

from plugins.views import *

urlpatterns = [
    url(r'^avatar', api4get_avatar)
]