from enum import Enum, unique
import random


@unique
class BGColor(Enum):
    GAINSBORO = '#DCDCDC'
    SANDBEIGE = '#E6C3C3'
    MISTYROSE = '#FFE4E1'
    LINEN = '#FAF0E6'
    BISQUE = '#FFE4C4'
    LEMONCHIFFON = '#FFFACD'
    IVORY = '#FFFFF0'
    BEIGE = '#F5F5DC'
    HONEYDEW = '#F0FFF0'
    CREAM = '#FFFDD0'
    MINTCREAM = '#F5FFFA'
    LIGHTCYAN = '#E0FFFF'
    ALICEBLUE = '#F0F8FF'
    LAVENDER = '#E6E6FA'
    PAILLILAC = '#E6CFE6'
    BABYPINK = '#FFD9E6'
    PINK = '#FFC0CB'
    LAVENDERBLUSH = '#FFF0F5'

    @staticmethod
    def random():
        color = random.choice(list(BGColor))
        return color.value


@unique
class FGColor(Enum):
    BLACK = '#000000'
    RED = '#FF0000'
    CERISE = '#DE3163'
    EMERALD = '#50C878'
    SEPIA = '#704214'
    BURNTORANGE = '#CC5500'
    CORAL = '#FF7F50'
    MAROON = '#800000'
    INDIGO = '#4B0080'
    SPRINGGREEN = '#00FF80'
    ALIZARINCRIMSON = '#E32636'
    DODGERBLUE = '1E90EF'
    CERULEANBLUE = '#2A52BE'
    GREEN = '#00FF00'
    YELLOW = '#FFFF00'
    AMBER = '#FFBF00'
    SCARLET = '#FF2400'
    MAUVE = '#6640FF'
    TEAL = '#008080'
    TURQUOISE = '#30D5C8'
    TANGERINE = '#F28500'
    VERMILION = '#FF4D00'

    @staticmethod
    def random():
        color = random.choice(list(FGColor))
        return color.value
