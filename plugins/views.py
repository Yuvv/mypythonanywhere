from django.shortcuts import render, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from PIL import Image, ImageDraw, ImageFont

from plugins.models import Avatar
from plugins.utils import BGColor, FGColor
from mysite.settings import MEDIA_ROOT


def api4get_avatar(req):
    u_name = req.GET.get('sn', 'Mu') + 'Mu'
    u_name = u_name[:2].capitalize()
    try:
        avatar = Avatar.objects.get(sn=u_name)
        with open(MEDIA_ROOT + avatar.uri, 'rb') as f:
            return HttpResponse(f.read(), content_type='image/jpeg')
    except ObjectDoesNotExist:
        im = Image.new('RGB', (128, 128), BGColor.random())
        fnt = ImageFont.truetype(MEDIA_ROOT + '/font/AnonymousPro.ttf', 96)
        dr = ImageDraw.Draw(im)
        dr.text((16, 16), u_name, font=fnt, fill=FGColor.random())
        uri = '/image/avatar/%s.jpg' % u_name
        im.save(MEDIA_ROOT + uri)
        avatar = Avatar(sn=u_name, uri=uri)
        avatar.save()
        resp = HttpResponse(content_type='image/jpeg')
        im.save(resp, 'JPEG')
        return resp


