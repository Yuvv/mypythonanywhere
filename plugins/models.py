from django.db import models


class Avatar(models.Model):
    sn = models.CharField(primary_key=True, max_length=2)   # short name
    uri = models.URLField()
