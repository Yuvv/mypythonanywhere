# -*- coding: utf-8 -*-
from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.contrib import auth
from django.db.transaction import atomic
from django.contrib.auth.decorators import login_required
from django.db.models import Avg, Count
import json

from myclass.models import *


@login_required(login_url='/login')
@atomic
def rating(req):
    # return render(req, 'rate_debug.html')
    if req.method == 'POST':
        scores = json.loads(req.POST.get('scores'))
        cadres = req.POST.getlist('cadres[]')
        try:
            for up in UserProfile.objects.only('user').all():
                rs = RateScore(me=req.user,
                               other=up.user,
                               score=scores.get(str(up.user.id)))
                rs.save()
            for cadre in cadres:
                gc = GoodCadre(me=req.user,
                               them=User.objects.get(id=cadre))
                gc.save()
        except Exception as ex:
            return HttpResponse(ex, status=401)
        return HttpResponse('ok')
    ups = UserProfile.objects.only('user_id', 'user__username').order_by('id').all()
    cadres = UserProfile.objects.only('user_id', 'user__username').order_by('id').filter(cadre=True)
    return render(req, 'rate.html', {'ups': ups,
                                     'cadres': cadres})


@login_required(login_url='/login')
def rate_view(req):
    rss_o = RateScore.objects.only('other__username', 'score').order_by('other_id').filter(me=req.user)
    abstract = UserProfile.objects.only('meeting', 'c_event', 'attached', 'r_event').get(user=req.user)
    rss_m = RateScore.objects.filter(other=req.user).aggregate(Avg('score'), Count('score'))
    if not rss_m.get('score__avg'):
        rss_m['score__avg'] = 0
    r_score = rss_m.get('score__avg') / 100 * 3
    t_score = min(10., r_score + abstract.meeting + abstract.c_event + abstract.r_event + abstract.attached)
    return render(req, 'rate_view.html', {'rss_o': rss_o,
                                          'abstract': abstract,
                                          'rss_m': rss_m,
                                          'r_score': r_score,
                                          't_score': t_score})


@login_required(login_url='/login')
def result_view(req):
    rss = list(RateScore.objects.order_by('other_id').values('other__username').annotate(Avg('score'), Count('score')))
    gcs = GoodCadre.objects.values('them__username').annotate(Count('me'))
    for i, up in enumerate(UserProfile.objects.order_by('id').only('meeting', 'c_event', 'attached', 'r_event').all()):
        rss[i]['score__sum'] = min(10.,
                                   up.meeting + up.c_event + up.r_event + up.attached + rss[i]['score__avg'] / 100 * 3)

    return render(req, 'result_view.html', {'rss': rss,
                                            'gcs': gcs})


def user_login(req):
    if req.method == 'POST':
        sno = req.POST.get('sno').upper()
        pwd = req.POST.get('pwd')
        up = UserProfile.objects.get(stu_no=sno)
        if up:
            u = auth.authenticate(username=up.user.username,
                                  password=pwd)
            if u:
                auth.login(req, u)
                return HttpResponseRedirect(req.GET.get('next', '/rate'))
        return HttpResponse('账号或密码错误！', status=401)
    return render(req, 'login.html')


def user_logout(req):
    auth.logout(req)
    return HttpResponseRedirect(req.GET.get('next', '/rate'))


def go_back_statistic(req):
    if req.method == 'POST':
        try:
            stu = GoAndBack.objects.get(stu_no=req.POST.get('stuNo'))
        except GoAndBack.DoesNotExist:
            stu = GoAndBack()
        stu.stu_no = req.POST.get('stuNo')
        stu.name = req.POST.get('name')
        stu.phone = req.POST.get('phone')
        stu.go_time = req.POST.get('goTime')
        stu.back_time = req.POST.get('backTime')
        stu.where = req.POST.get('where')
        stu.remark = req.POST.get('remark')
        stu.filled = True
        stu.save()
        return HttpResponse('OK')
    return render(req, 'go_back.html')


def go_back_get_stu(req, stu_no):
    try:
        stu = GoAndBack.objects.get(stu_no=stu_no)
        return HttpResponse(json.dumps(stu.to_json()), content_type='application/json')
    except Exception as ex:
        return HttpResponse('╮(╯▽╰)╭\n系统中没有找到你!\n╮(╯▽╰)╭', status=404)
