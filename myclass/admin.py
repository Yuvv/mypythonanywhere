from django.contrib import admin

from myclass.models import *


@admin.register(RateScore)
class RateScoreAdmin(admin.ModelAdmin):
    # list_display = ['me', 'other', 'score']
    pass


@admin.register(GoodCadre)
class GoodCadreAdmin(admin.ModelAdmin):
    pass


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(GoAndBack)
class GoAndBackAdmin(admin.ModelAdmin):
    list_display = ['stu_no', 'name', 'where', 'go_time', 'back_time', 'filled']
    list_filter = ['filled', 'where']
