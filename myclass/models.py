# -*- coding: utf-8 -*-
from django.db import models

from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    stu_no = models.CharField(max_length=9, unique=True, help_text='学号')
    phone = models.CharField(max_length=11, help_text='手机')
    birthday = models.DateField(help_text='生日')
    dormitory = models.CharField(max_length=7, help_text='宿舍')
    id_no = models.CharField(max_length=18, help_text='身份证号')
    political_status = models.CharField(max_length=8, help_text='政治面貌')

    cadre = models.BooleanField(default=False, help_text='是否为班干')
    meeting = models.FloatField(default=0, help_text='班会（总计4次）')
    r_event = models.FloatField(default=0, help_text='科研活动')
    c_event = models.FloatField(default=0, help_text='团建活动（总计3次）')
    attached = models.FloatField(default=0, help_text='附加分数')

    def __str__(self):
        return '%s  %s' % (
            self.stu_no,
            self.user.username
        )


class RateScore(models.Model):
    me = models.ForeignKey(User, related_name='%(class)s_me', on_delete=models.CASCADE)
    other = models.ForeignKey(User, related_name='%(class)s_other', on_delete=models.CASCADE)
    score = models.IntegerField()

    def __str__(self):
        return '%s rate %s with %d' % (
            self.me.username,
            self.other.username,
            self.score
        )

    class Meta:
        unique_together = ('me', 'other')


class GoodCadre(models.Model):
    me = models.ForeignKey(User, related_name='%(class)s_me', on_delete=models.CASCADE)
    them = models.ForeignKey(User, related_name='%(class)s_them', on_delete=models.CASCADE)

    def __str__(self):
        return '%s rate %s' % (
            self.me.username,
            self.them.username
        )

    class Meta:
        unique_together = ('me', 'them')


class GoAndBack(models.Model):
    """离返校情况"""
    stu_no = models.CharField(max_length=9, primary_key=True)
    name = models.CharField(max_length=16, default='no name')
    phone = models.CharField(max_length=11, default='18800000000')
    go_time = models.CharField(max_length=4, default='1001')
    back_time = models.CharField(max_length=4, default='1009')
    where = models.CharField(max_length=32, default='实验科研')
    remark = models.CharField(max_length=255, default='无')
    filled = models.BooleanField(default=False)

    def __str__(self):
        return self.stu_no + ' - ' + self.name

    def to_json(self):
        return {'stuNo': self.stu_no,
                'name': self.name,
                'phone': self.phone,
                'goTime': self.go_time,
                'backTime': self.back_time,
                'where': self.where,
                'remark': self.remark,
                'filled': self.filled}
