"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

import myclass.views as myc_view

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^waiting-for-death/', myc_view.go_back_statistic),
    url(r'^go-back/get/(?P<stu_no>[sSgG]\d{8})$', myc_view.go_back_get_stu),
    url(r'^rate/view', myc_view.rate_view),
    url(r'^result/view', myc_view.result_view),
    url(r'^rate', myc_view.rating),
    url(r'^login$', myc_view.user_login),
    url(r'^logout$', myc_view.user_logout),
    url(r'^api/plugins/', include('plugins.urls'))
]
